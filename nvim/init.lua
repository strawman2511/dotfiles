-- Use plugins directory
require("plugins")

-- Use plugins
require("lualine").setup()

-- Use monokai theme
require('monokai').setup {}
require('monokai').setup { palette = require('monokai').pro }
require('monokai').setup { palette = require('monokai').soda }
require('monokai').setup { palette = require('monokai').ristretto }

-- Set line number and enable syntax
vim.wo.number = true
vim.o.syntax = 'on'
