-- This file can be loaded by calling `lua require('plugins')` from your init.vim

-- Only required if you have packer configured as `opt`
vim.cmd [[packadd packer.nvim]]

return require('packer').startup(function(use)
  -- Packer can manage itself
  use 'wbthomason/packer.nvim'

  -- lualine statusline
  use 'nvim-lualine/lualine.nvim'
  requires = { 'kyazdani42/nvim-web-devicons' }

  -- Monokai theme
  use 'tanvirtin/monokai.nvim'
end)
